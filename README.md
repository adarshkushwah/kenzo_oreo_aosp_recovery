# kenzo_oreo_aosp_recovery
This repository holds the compiled AOSP-Oreo (Android 8.0) recovery for the Android device Xiaomi Redmi note 3 pro.

The following tutorial is mailnly being introduced for the sake of booting this image as the stock recovery isn't fun at all.
But you may do as you please. Just replace the command in line #2 with : <CODE>fastboot flash recovery recovery.img</CODE>.

# Follow the instructions:-
1. Download the Minimal adb and fastboot tool.
2. Download this recovery image and paste it inside the Minimal folder's location.
3. Connect your device and make sure the usb debugging is on.
4. Type the following commands in the sequence:-
   
   1. <CODE>adb reboot bootloader</CODE>
   2. <CODE>fastboot boot recovery.img</CODE>
   3. <CODE>fastboot reboot</CODE>
   
   The new recovery will boot-up temporarily.
